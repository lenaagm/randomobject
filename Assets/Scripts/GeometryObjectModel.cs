﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeometryObjectModel : MonoBehaviour {

    private GameData gameData;

    public int clickCount = 0;
    private Color cubeColor;

    void Start()
    {

        gameData = Resources.Load<GameData>("GameData");
        InvokeRepeating("ChangeColor", gameData.ObservableTime, gameData.ObservableTime);
    }

    private void ChangeColor()
    {
        if (!TouchScreen.isClickColor)
        {
            cubeColor = new Color((float)mRandom.GetRandomNumber(0, 1f), (float)mRandom.GetRandomNumber(0, 1f), (float)mRandom.GetRandomNumber(0, 1f), 1f);
            gameObject.GetComponent<MeshRenderer>().material.color = cubeColor;
        }
    }

    class mRandom
    {
        public static System.Random random = new System.Random();
        public static double GetRandomNumber(double minimum, double maximum)
        {
            return random.NextDouble() * (maximum - minimum) + minimum;
        }
    }
}