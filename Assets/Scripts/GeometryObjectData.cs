﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GeometryObjectData", menuName = "GeometryObjectData")]
public class GeometryObjectData : ScriptableObject {

    //public int 
    public List<ClickColorData> ClicksData;

}