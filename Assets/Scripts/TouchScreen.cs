﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class TouchScreen : MonoBehaviour {

    public GameObject[] geometryObjects;
    private GeometryObjectData geometryObjectsData;
    private GameObject geometryObject;

    private NameObject objectNames;

    private bool isGeometryObject = false;
    private int rand = 0;

    public static bool isClickColor;

    private void Awake()
    {
        geometryObjectsData = Resources.Load<GeometryObjectData>("GeometryObjectData");

        rand = UnityEngine.Random.Range(0, 3);
    }
	
	void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            if (!isGeometryObject)
            {

                TextAsset file = Resources.Load("pref_names") as TextAsset;
                string json = file.ToString();

                NameObject objectNames = JsonUtility.FromJson<NameObject>(json);

                //geometryObject = Instantiate(geometryObjects[rand]);

                string objType = geometryObjectsData.ClicksData[rand].ObjectType;

                switch (objType)
                {
                    case "Cube":
                        geometryObject = Instantiate(geometryObjects[0]);
                        geometryObject.name = objectNames.Cube;
                        break;
                    case "Sphere":
                        geometryObject = Instantiate(geometryObjects[1]);
                        geometryObject.name = objectNames.Sphere;
                        break;
                    case "Capsule":
                        geometryObject = Instantiate(geometryObjects[2]);
                        geometryObject.name = objectNames.Capsule;
                        break;
                }

                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out hit))
                {
                    geometryObject.transform.position = new Vector3(hit.point.x, hit.point.y, 60);
                }

                isGeometryObject = true;
            } else
            {
                RaycastHit hit;
                Ray ray = gameObject.GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);

                if (Physics.Raycast(ray, out hit) && hit.transform.gameObject.tag.Equals("GeometryObject"))
                {
                    geometryObject.GetComponent<GeometryObjectModel>().clickCount += 1;
                }
            }
        }

        if (geometryObject != null)
        {
            if (geometryObjectsData.ClicksData[rand].MinClicksCount <= geometryObject.GetComponent<GeometryObjectModel>().clickCount &&
                geometryObjectsData.ClicksData[rand].MaxClicksCount >= geometryObject.GetComponent<GeometryObjectModel>().clickCount)
            {
                if (!isClickColor)
                {
                    geometryObject.GetComponent<MeshRenderer>().material.color = geometryObjectsData.ClicksData[rand].Color;
                    isClickColor = true;
                }

            } else if (isClickColor)
            {
                isClickColor = false;
            }
        }
    }

    [Serializable]
    public class NameObject
    {
        public string Cube;
        public string Sphere;
        public string Capsule;
    }
}